# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bcano <bcano@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/12 14:25:17 by jraffin           #+#    #+#              #
#    Updated: 2022/01/28 18:38:28 by bcano            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL			=	/bin/sh

PROGNAME		:=	minishell

LIBFT			:=	libft/libft.a

INCLUDEDIR		:=	inc
SRCDIR			:=	src

OBJDIR			:=	./obj
DEBUGDIR		:=	./debugobj

SRCS			:=	main.c						\
					ms_utils.c					\
					sighandlers.c				\
					launcher/launcher.c			\
					launcher/redirections.c		\
					launcher/expand_heredoc.c	\
					launcher/parenthesis.c		\
					launcher/commands.c			\
					launcher/execve.c			\
					launcher/pipes.c			\
					launcher/tk_list_to_argv.c	\
					parsing/parser.c			\
					parsing/parser_utils.c		\
					parsing/ast_utils.c			\
					parsing/heredoc.c			\
					lexing/lexer.c				\
					lexing/token_basics.c		\
					lexing/token_expand.c		\
					lexing/token_utils.c		\
					lexing/tokens.c				\
					lexing/tokens2.c			\
					lexing/tokens3.c			\
					lexing/wildcards.c			\
					lexing/mergesort.c			\
					env/chained_add.c			\
					env/chained_delete.c		\
					env/chained_edit.c			\
					env/chained_get_params.c	\
					env/chained_print.c			\
					env/chained_to_tab.c		\
					builtin/cd.c				\
					builtin/echo.c				\
					builtin/env.c				\
					builtin/exit.c				\
					builtin/export.c			\
					builtin/parse.c				\
					builtin/pwd.c				\
					builtin/unset.c				\
					builtin/utils.c

CC				:=	cc
RM				:=	rm

CCFLAGS			:=	-Wall -Wextra -Werror -MMD -MP
LIBFLAGS		:=	-lreadline
OPTFLAG			:=

NAME			:=	$(PROGNAME)

OUTDIR			:=	$(OBJDIR)

DEBUGNAME		:= $(addsuffix .debug,$(PROGNAME))

ifdef DEBUG
	OPTFLAG 	:=	-g
	LIBFT		:=	$(addsuffix .debug,$(LIBFT))
	NAME		:=	$(DEBUGNAME)
	OUTDIR		:=	$(DEBUGDIR)
endif

$(OUTDIR)/%.o	:	$(SRCDIR)/%.c | $(OUTDIR)
	@mkdir -p $(dir $@)
	$(CC) -c $(CCFLAGS) $(OPTFLAG) $(SANITIZE) $(addprefix -I ,$(INCLUDEDIR)) $(addprefix -I ,$(dir $(LIBFT))) $< -o $@

$(NAME)		:	$(addprefix $(OUTDIR)/,$(SRCS:.c=.o)) $(LIBFT)
	$(CC) $(CCFLAGS) $(OPTFLAG) -o $(NAME) $(addprefix $(OUTDIR)/,$(SRCS:.c=.o)) $(LIBFT) $(LIBFLAGS)

all	:	$(NAME)
		@touch ~/.reset		

minishell	:	$(NAME)

$(DEBUGNAME)	:
ifndef DEBUG
	$(MAKE) minishell $(DEBUGNAME) DEBUG=1
endif

debug	:
ifndef DEBUG
	$(MAKE) minishell DEBUG=1
endif

ifdef LIBFT
$(LIBFT)	:
	$(MAKE) -C $(dir $(LIBFT)) $(notdir $(LIBFT))
endif

$(OUTDIR)	:
	mkdir $(OUTDIR)

clean	:
ifdef LIBFT
	$(MAKE) -C $(dir $(LIBFT)) fclean
endif
	$(RM) -rf $(OBJDIR) $(DEBUGDIR)

fclean				:	clean
	$(RM) -f $(PROGNAME) $(DEBUGNAME)

re	:	fclean $(NAME)

-include	:	$(OUTDIR:.o=.d))

.PHONY		:	all clean fclean re
