# Minishell V.6
As beautiful as a shell

The objective of this project is for you to create a simple shell. Yes, your
little bash or zsh. You will learn a lot about processes and file descriptors.

This is 42 school's minishell project. Made with [jonathan raffin](https://github.com/rivenbis).

:warning: **Warning**: It is educational project.

:warning: **Warning**: You can take inspiration from it but please don't copy / paste what you don't understand.

### Shell should do :

#### Mandatory part:

- Not interpret unclosed quotes or unspecified special characters like \ or ;.
- Not use more than one global variable, think about it and be ready to explain why you do it.
- Show a prompt when waiting for a new command.
- Have a working History.
- Search and launch the right executable (based on the PATH variable or by using relative or absolute path)
- It must implement the builtins:
  - echo with option -n
  - cd with only a relative or absolute path
  - pwd with no options
  - export with no options
  - unset with no options
  - env with no options or arguments
  - exit with no options
- ’ inhibit all interpretation of a sequence of characters.
- " inhibit all interpretation of a sequence of characters except for $.
- Redirections:
  - < should redirect input.
  - \> should redirect output.
  - “<<” read input from the current source until a line containing only the delimiter is seen. it doesn’t need to update history!
  - “>>” should redirect output with append mode.
- Pipes | The output of each command in the pipeline is connected via a pipe to the input of the next command.
- Environment variables ($ followed by characters) should expand to their values.
- $? should expand to the exit status of the most recently executed foreground
pipeline.
- ctrl-C ctrl-D ctrl-\ should work like in bash. Readline function can produce some leak you don’t need to fix this

#### Bonus part:

If the Mandatory part is not perfect don’t even think about bonuses
- &&, || with parenthesis for priorities.
- the wildcard * should work for the current working directory.  
  
    
      
Team minishell @jraffin @bcano - 2022
