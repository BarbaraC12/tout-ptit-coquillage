/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launcher.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/10 23:32:57 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 13:16:38 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LAUNCHER_H
# define LAUNCHER_H

# include "parsing.h"

/*	launcher.c			*/

int		launch_ast(t_ast_node *root, t_env_lst *env);
void	clean_exit(t_ast_node *ast, t_env_lst *env, int status);

/*	redirections.c		*/

int		resolve_redirection(t_ast_node *redir_node, t_env_lst *env);

/*	expand_heredoc.c	*/

int		expand_heredoc(int *fd, t_ast_node *heredoc_cond_node, t_env_lst *env);

/*	parenthesis.c		*/

int		resolve_parenthesis(t_ast_node *parenthesis_node, t_env_lst *env);

/*	commands.c			*/

int		resolve_command(t_ast_node *cmd_node, t_env_lst *env);
void	set_subproc(t_env_lst *env);

/*	execve.c			*/

int		execve_command(char **argv, t_env_lst *env);

/*	pipes.c				*/

int		is_piped(t_ast_node *node);
int		is_piped_last(t_ast_node *node);
void	wait_pipeline(t_ast_node *last_piped_node, int *wstatus);
void	close_all_node_fds(t_ast_node *node);

/*	tk_list_to_argv.c	*/

char	**tk_list_to_argv(t_tk_list *tk_list);

#endif
