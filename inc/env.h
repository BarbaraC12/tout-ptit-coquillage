/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:18:11 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 14:02:00 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_H
# define ENV_H

# include <stdlib.h>

/* *****************************************
**				ENV CHAINED LIST
** *************************************** */

typedef struct s_env_lst	t_env_lst;
typedef struct s_env_node	t_env_node;

struct		s_env_lst
{
	t_env_node	*head;
	t_env_node	*tail;
	size_t		lst_size;
};

struct		s_env_node
{
	char		*env;
	char		*name;
	char		*value;
	t_env_node	*next;
	t_env_node	*prev;
};

/* *****************************************
**	Create new list and node
** *************************************** */
t_env_node	*new_env_node(char *env, char *name, char *value);
void		push_back_env_lst(
				t_env_lst *const lst, t_env_node *const node);
int			add_back_env_lst(
				t_env_lst **const lst, char *env, char *name, char *value);
int			set_env(char **envp, t_env_lst *lst);

/* *****************************************
**	Delete node and list
** *************************************** */
void		clear_env_lst(t_env_lst *const lst);
void		error_clear_env_lst(t_env_lst **const lst);
void		delone_env_lst(
				t_env_lst *const lst, t_env_node *node);

/* *****************************************
**	Get params from chained list
** *************************************** */
char		*get_name(char *const str);
char		*get_value(char const *str);
size_t		size_tab(char **tab);
char		*get_venv(t_env_lst const *lst, const char *name);
char		*get_venv_safe(t_env_lst const *lst, const char *name);

/* *****************************************
**	Edit or add new node
** *************************************** */
int			env_replace_value(
				char *name, t_env_lst *lst, char *new_value);
int			set_new_pwd(
				t_env_lst *lst, char *new_value, char *last_value);

/* *****************************************
**	Print list or specific node
** *************************************** */
void		print_env_lst(t_env_lst const *lst);
void		print_env_node(t_env_node const *const node);

/* ****************************************
**			TRASLATE CHAINED TO ARRAY
** *************************************** */
char		**lst_to_tab(t_env_lst *lst);

#endif
