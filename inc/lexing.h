/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/12 17:45:35 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/25 19:07:12 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXING_H
# define LEXING_H

# include <stddef.h>
# include "env.h"

/*
	<token type>		<exec node>	<expandable>	<meaning>
0	LX_TK_EXIT_STATUS,					yes			$?
1	LX_TK_VARIABLE,						yes			$[a-zA-Z_][a-zA-Z0-9_]*
		(expands as token list with compressed spaces and wildcards if not quoted)
2	LX_TK_WILDCARD,						yes			\*+
3	LX_TK_LITTERAL,									<any litteral string>
4	LX_TK_SPACE,									[ \f\n\r\t\v]+
5	LX_TK_HEREDOC,									<<
6	LX_TK_INPUT,									<
7	LX_TK_APPEND,									>>
8	LX_TK_OUTPUT,									>
9	LX_TK_PAR_CLOSE,								)
10	LX_TK_PAR_OPEN,		yes						(
11	LX_TK_PIPE				yes						|
12	LX_TK_AND,				yes						&&
13	LX_TK_OR,				yes						||
*/
enum	e_token_types
{
	LX_TK_EXIT_STATUS,
	LX_TK_VARIABLE,
	LX_TK_WILDCARD,
	LX_TK_LITTERAL,
	LX_TK_SPACE,
	LX_TK_HEREDOC,
	LX_TK_INPUT,
	LX_TK_APPEND,
	LX_TK_OUTPUT,
	LX_TK_PAR_CLOSE,
	LX_TK_PAR_OPEN,
	LX_TK_PIPE,
	LX_TK_AND,
	LX_TK_OR
};

typedef struct s_token_list
{
	int					type;
	int					is_quoted;
	char				*value;
	struct s_token_list	*next;
}	t_tk_list;

/*	lexer.c				*/

t_tk_list	*build_tokens_list(char *str);

/*	token_basics.c		*/

t_tk_list	*create_token(int type, const char *value, size_t len);
t_tk_list	*free_token_list(t_tk_list *tk_list);
const char	*get_tk_prefix(int tk_type);

/*	token_utils.c		*/

void		remove_leading_space_tokens(t_tk_list **tk_list);
t_tk_list	*detach_first_token(t_tk_list **tk_list);
t_tk_list	*detach_tk_group(t_tk_list **tk_list);
int			append_tk_list(t_tk_list **tk_list, t_tk_list *tk_list_to_append);
int			insert_at_spot_and_point_next(t_tk_list ***spot, t_tk_list *list);

/*	tokens.c			*/

t_tk_list	*get_tk_litteral(char **str, char *until_chars, int is_quoted);
t_tk_list	*get_if_tk_exit_status(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_variable(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_litteral_dollar(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_wildcard(char **str, t_tk_list **token);

/*	tokens2.c			*/

t_tk_list	*get_if_tk_space(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_heredoc(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_input(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_append(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_output(char **str, t_tk_list **token);

/*	tokens3.c			*/

t_tk_list	*get_if_tk_and(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_or(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_pipe(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_left_parenthesis(char **str, t_tk_list **token);
t_tk_list	*get_if_tk_right_parenthesis(char **str, t_tk_list **token);

/*	token_expand.c		*/

char		*unexpand_tk_list(t_tk_list *tk_group);
int			expand_wildcards_in_token_list(t_tk_list **tk_list);
int			expand_variables_in_token_list(t_tk_list **tk_list, t_env_lst *env);

/*	wildcards.c			*/

t_tk_list	*get_cwd_wildcard_matching_tokens(char *wild);

/*	mergesort.c			*/

int			mergesort_litteral_tk_list(t_tk_list **litteral_tk_list);

#endif
