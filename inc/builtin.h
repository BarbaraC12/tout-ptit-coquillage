/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:08:37 by bcano             #+#    #+#             */
/*   Updated: 2022/01/28 16:42:30 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTIN_H
# define BUILTIN_H

enum	e_builtin
{
	NOT_BUILTIN,
	BUILTIN_EXIT,
	BUILTIN_PWD,
	BUILTIN_CD,
	BUILTIN_ECHO,
	BUILTIN_EXPORT,
	BUILTIN_UNSET,
	BUILTIN_ENV,
};

typedef struct s_utl		t_cd_utl;
typedef struct s_env_lst	t_env_lst;
typedef struct s_env_node	t_env_node;
typedef struct s_ast_node	t_ast_node;

struct		s_utl
{
	char	*home;
	char	*pwd;
	char	*oldpwd;
	char	*cdpath;
	char	*cd;
};

char		*skip_dollars(const char *str);

char		*path_name(char *cdpath, char *dir);
char		*real_path(char *path, char *dir);

char		*echo_env(t_env_lst const *lst, char *name);

int			is_builtin(char *arg);
int			run_builtin(char **argv,		
				t_env_lst *lst, int out, t_ast_node *ast);

int			builtin_pwd(char **argv, int out);
int			builtin_cd(char **argv, t_env_lst *lst);
int			builtin_echo(char **argv, int out);
int			builtin_export(char **argv, t_env_lst *lst);
int			builtin_unset(char **argv, t_env_lst *lst);
int			builtin_env(char **argv, t_env_lst *lst);
int			builtin_exit(char **argv, t_env_lst *lst,	
				t_ast_node *ast);

#endif