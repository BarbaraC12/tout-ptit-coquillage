/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 14:13:19 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 09:13:04 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSING_H
# define PARSING_H

# include <sys/types.h>
# include "lexing.h"

typedef struct s_ast_node
{
	struct s_ast_node	*parent;
	struct s_ast_node	*left;
	struct s_ast_node	*right;
	t_tk_list			*tk_list;
	int					in_fd;
	int					out_fd;
	pid_t				pid;
}	t_ast_node;

/*	parser.c			*/

t_ast_node	*build_ast(t_tk_list **token_input);
int			unexpected_token(t_tk_list *tk_list);

/*	parser_utils.c		*/

void		set_pipe_between(t_ast_node *write_node, t_ast_node *read_node);
int			create_heredoc(t_tk_list *tk_group);
int			parse_all_open_parenthesis(t_ast_node **node, t_tk_list **tk_input);
int			parse_redirection(t_ast_node **node, t_tk_list **tk_input);
int			parse_closed_parenthesis(t_ast_node **node, t_tk_list **tk_input);

/*	ast_utils.c			*/

t_ast_node	*create_ast_node(void);
t_ast_node	*free_ast_node(t_ast_node *node);
t_ast_node	*free_whole_ast(t_ast_node *node);
int			insert_new_node_pair_upright(t_ast_node *node);
int			insert_new_node_pair_downleft(t_ast_node *node);

#endif
