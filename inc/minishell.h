/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 15:38:13 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 22:56:30 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# define MSH "minishell"

extern int	g_status;

/*	ms_utils.c		*/

int		get_g_status(void);
void	update_g_status(int wstatus);
int		ms_put_error(char *path, char *obj, char *msg, int ret_val);
void	*ms_ptr_error(char *path, char *obj, char *msg, void *ret_ptr);

/*	sighandlers.c	*/

void	interactive_sighandler(int sig);
void	executive_sighandler(int sig);
void	heredoc_sighandler(int sig);
void	sigquit_handler(int sig);

#endif
