/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_expand.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/04 04:34:06 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 11:07:10 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "minishell.h"
#include "parsing.h"
#include "env.h"

/**
 * @brief				Converts space separated token value groups back to
 * 						original text value (without quotes).
 *
 * @param[in,out]		tk_list	Token value group
 * @return				pointer to the mallocated resulting string.
 */
char	*unexpand_tk_list(t_tk_list *tk_list)
{
	char	*str;
	char	*freestr;

	str = malloc(sizeof(char));
	if (!str)
		return (ms_ptr_error(NULL, NULL, NULL, NULL));
	*str = '\0';
	while (tk_list)
	{
		freestr = str;
		str = ft_strjoin3(str, get_tk_prefix(tk_list->type), tk_list->value);
		free(freestr);
		if (!str)
			return (ms_ptr_error(NULL, NULL, NULL, NULL));
		tk_list = tk_list->next;
	}
	return (str);
}

/**
 * @brief				Relexes a token value of type LX_TK_VARIABLE for spaces
 * 						and wildcards and swaps it in place with the
 * 						corresponding created group of tokens.
 *
 * @param[in,out]	token	LX_TK_VARIABLE token to expand.
 * @return					0 on success, 1 on error.
 */
static int	relex_token_value(t_tk_list **token)
{
	t_tk_list	*rest;
	char		*str;
	int			err;

	rest = *token;
	str = rest->value;
	if (!str)
		return (ms_put_error(NULL, "relex_token_value", "value-less token", 1));
	while (*str)
	{
		if (!get_if_tk_space(&str, token)
			&& !get_if_tk_wildcard(&str, token))
			*token = get_tk_litteral(&str, " *", 0);
		if (!*token)
			break ;
		token = &(*token)->next;
	}
	*token = rest->next;
	err = !!*str;
	free(rest->value);
	free(rest);
	return (err);
}

/**
 * @brief			Expands all value token groups (of typea < LX_TK_SPACE)
 * 					that contains one or more wildcard token (LX_TK_WILDCARD)
 * 					into a list of space separated litteral tokens containins
 * 					the matching filenames in the current working directory.
 * 					If no matching filename is found, returns only one litteral
 * 					token containing the matching pattern.
 *
 * @param[in,out]	tk_list		Token list where to expand variable tokens into.
 * @return						0 on success, 1 on error.
 */
int	expand_wildcards_in_token_list(t_tk_list **tk_list)
{
	t_tk_list	**list_ptr;
	t_tk_list	*new_tokens;
	char		*group_value;

	list_ptr = tk_list;
	while (*list_ptr)
	{
		if ((*list_ptr)->type >= LX_TK_SPACE)
		{
			list_ptr = &(*list_ptr)->next;
			continue ;
		}
		new_tokens = detach_tk_group(list_ptr);
		group_value = unexpand_tk_list(new_tokens);
		free_token_list(new_tokens);
		new_tokens = get_cwd_wildcard_matching_tokens(group_value);
		free(group_value);
		if (!new_tokens || insert_at_spot_and_point_next(&list_ptr, new_tokens))
			return (1);
	}
	return (0);
}

/**
 * @brief				Expands all variable tokens (type <=LX_TK_QUOTED_VAR)
 * 						in a token list.
 *
 * @param[in,out]	tk_list		Token list where to expand variable tokens into.
 * @return						0 on success, 1 on error.
 */
int	expand_variables_in_token_list(t_tk_list **tk_list, t_env_lst *env)
{
	t_tk_list	**token;
	char		*new_value;

	token = tk_list;
	while (*token)
	{
		if ((*token)->type <= LX_TK_VARIABLE)
		{
			if ((*token)->type == LX_TK_EXIT_STATUS)
				new_value = ft_itoa(g_status);
			else
				new_value = ft_strdup(get_venv_safe(env, (*token)->value));
			if (!new_value)
				return (ms_put_error(NULL, NULL, NULL, 1));
			free((*token)->value);
			(*token)->value = new_value;
			if ((*token)->type != LX_TK_VARIABLE || (*token)->is_quoted)
				(*token)->type = LX_TK_LITTERAL;
			else if (relex_token_value(token))
				return (1);
		}
		if (*token)
			token = &(*token)->next;
	}
	return (0);
}
