/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 14:47:38 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 11:20:01 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "minishell.h"

static void	adjust_active_quotes(char **str, int *active_quotes)
{
	if ((*active_quotes == 1 && **str == '\'')
		|| (*active_quotes == 2 && **str == '\"'))
	{
		*active_quotes = 0;
		(*str)++;
		return ;
	}
	if (!*active_quotes)
	{
		if (**str == '\'')
		{
			*active_quotes = 1;
			(*str)++;
		}
		else if (**str == '\"')
		{
			*active_quotes = 2;
			(*str)++;
		}
		return ;
	}
}

static t_tk_list	*get_next_token(char **str, int *active_quotes)
{
	t_tk_list	*token;

	token = NULL;
	adjust_active_quotes(str, active_quotes);
	if (!**str)
		return (NULL);
	if (*active_quotes == 1)
		return (get_tk_litteral(str, "\'", *active_quotes));
	if (get_if_tk_exit_status(str, &token)
		|| get_if_tk_variable(str, &token)
		|| get_if_tk_litteral_dollar(str, &token))
		return (token->is_quoted = *active_quotes, token);
	if (*active_quotes == 2)
		return (get_tk_litteral(str, "$\"", *active_quotes));
	if (get_if_tk_space(str, &token)
		|| get_if_tk_wildcard(str, &token)
		|| get_if_tk_heredoc(str, &token) || get_if_tk_input(str, &token)
		|| get_if_tk_append(str, &token) || get_if_tk_output(str, &token)
		|| get_if_tk_and(str, &token) || get_if_tk_or(str, &token)
		|| get_if_tk_pipe(str, &token)
		|| get_if_tk_left_parenthesis(str, &token)
		|| get_if_tk_right_parenthesis(str, &token))
		return (token->is_quoted = *active_quotes, token);
	return (get_tk_litteral(str, NULL, *active_quotes));
}

static void	display_error(int active_quotes, int parenthesis)
{
	if (active_quotes == 1)
		ms_put_error(NULL, "'", "missing `''", 0);
	if (active_quotes == 2)
		ms_put_error(NULL, "\"", "missing `\"'", 0);
	if (parenthesis > 0)
		ms_put_error(NULL, "(", "missing `)'", 0);
	if (parenthesis < 0)
		ms_put_error(NULL, NULL, "syntax error near unexpected token `)'", 0);
}

t_tk_list	*build_tokens_list(char *str)
{
	t_tk_list	*root;
	t_tk_list	*last;
	int			active_quotes;
	int			parenthesis;

	parenthesis = 0;
	active_quotes = 0;
	root = get_next_token(&str, &active_quotes);
	last = root;
	while (last)
	{
		if (last->type == LX_TK_PAR_OPEN)
			parenthesis++;
		if (last->type == LX_TK_PAR_CLOSE)
			parenthesis--;
		last->next = get_next_token(&str, &active_quotes);
		last = last->next;
	}
	if (!active_quotes && !parenthesis)
		return (root);
	root = free_token_list(root);
	display_error(active_quotes, parenthesis);
	g_status = 2;
	return (NULL);
}
