/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mergesort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/12 12:48:18 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/15 10:02:28 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"
#include "minishell.h"

static t_tk_list	*merge_lists(t_tk_list *list1, t_tk_list *list2)
{
	t_tk_list	*merged;
	t_tk_list	**end;

	merged = NULL;
	end = &merged;
	while (list1 && list2)
	{
		if (ft_strcmp(list1->value, list2->value) < 0)
		{
			*end = list1;
			list1 = list1->next;
		}
		else
		{
			*end = list2;
			list2 = list2->next;
		}
		end = &(*end)->next;
	}
	if (list1)
		*end = list1;
	else
		*end = list2;
	return (merged);
}

static int	list_len_and_check(t_tk_list *list, size_t *len)
{
	*len = 0;
	while (list)
	{
		if (list->type != LX_TK_LITTERAL)
			return (1);
		(*len)++;
		list = list->next;
	}
	return (0);
}

static void	fill_array_with_unlinked_nodes(
								t_tk_list **array, t_tk_list *list, size_t len)
{
	size_t		i;

	i = 0;
	while (i < len)
	{
		array[i] = list;
		list = list->next;
		array[i]->next = NULL;
		i++;
	}
}

int	mergesort_litteral_tk_list(t_tk_list **litteral_tk_list)
{
	t_tk_list	**array;
	size_t		len;
	size_t		gap;
	size_t		i;

	if (list_len_and_check(*litteral_tk_list, &len))
		return (1);
	array = malloc(sizeof(t_tk_list *) * len);
	if (!array)
		return (ms_put_error(NULL, NULL, NULL, 1));
	fill_array_with_unlinked_nodes(array, *litteral_tk_list, len);
	gap = 1;
	while (gap < len)
	{
		i = 0;
		while ((i + gap) < len)
		{
			array[i] = merge_lists(array[i], array[i + gap]);
			i += gap << 1;
		}
		gap <<= 1;
	}
	*litteral_tk_list = array[0];
	free(array);
	return (0);
}
