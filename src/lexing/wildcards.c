/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wildcards.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/11 08:20:54 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/28 15:50:29 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <errno.h>
#include "libft.h"
#include "minishell.h"
#include "parsing.h"

static inline	int	init_cwd(DIR **cwd)
{
	char	dirpath[PATH_MAX];

	if (!getcwd(dirpath, PATH_MAX))
		return (1);
	*cwd = opendir(dirpath);
	if (!*cwd)
		return (ms_put_error(NULL, NULL, NULL, 1));
	return (0);
}

static int	close_and_free(DIR *dir, t_tk_list **cwd_list)
{
	closedir(dir);
	*cwd_list = free_token_list(*cwd_list);
	return (1);
}

static int	get_cwd_litteral_tk_list(t_tk_list **cwd_list)
{
	DIR				*dir;
	struct dirent	*entry;
	t_tk_list		**last_spot;

	if (init_cwd(&dir))
		return (1);
	last_spot = cwd_list;
	errno = 0;
	entry = readdir(dir);
	while (entry)
	{
		*last_spot = create_token(
				LX_TK_LITTERAL, entry->d_name, ft_strlen(entry->d_name));
		if (!*last_spot)
			return (close_and_free(dir, cwd_list));
		last_spot = &(*last_spot)->next;
		entry = readdir(dir);
	}
	if (errno)
		return (ms_put_error(NULL, NULL, NULL, close_and_free(dir, cwd_list)));
	closedir(dir);
	return (mergesort_litteral_tk_list(cwd_list));
}

static int	is_wildcard_match(char *wild, char *str)
{
	char	*star;

	if (*str == '.' && *wild != '.')
		return (0);
	star = ft_strchrnul(wild, '*');
	if (ft_memcmp(wild, str, star - wild))
		return (0);
	str += star - wild;
	wild = star + !!*star;
	star = ft_strchr(wild, '*');
	while (star)
	{
		*star = '\0';
		str = ft_strstr(str, wild);
		*star = '*';
		if (!str)
			return (0);
		str += star - wild;
		wild = star + !!*star;
		star = ft_strchr(wild, '*');
	}
	if (ft_strlen(wild) > ft_strlen(str))
		return (0);
	str += ft_strlen(str) - ft_strlen(wild);
	return (!ft_strcmp(str, wild));
}

t_tk_list	*get_cwd_wildcard_matching_tokens(char *wild)
{
	t_tk_list	*cwd_list;
	t_tk_list	*file;
	t_tk_list	*result;

	if (!ft_strchr(wild, '*'))
		return (create_token(LX_TK_LITTERAL, wild, ft_strlen(wild)));
	result = NULL;
	if (get_cwd_litteral_tk_list(&cwd_list))
		return (create_token(LX_TK_LITTERAL, wild, ft_strlen(wild)));
	while (cwd_list)
	{
		file = detach_first_token(&cwd_list);
		if (!is_wildcard_match(wild, file->value))
			free_token_list(file);
		else if (append_tk_list(&result, file))
		{
			free_token_list(file);
			free_token_list(cwd_list);
			return (free_token_list(result));
		}
	}
	if (!result)
		result = create_token(LX_TK_LITTERAL, wild, ft_strlen(wild));
	return (result);
}
