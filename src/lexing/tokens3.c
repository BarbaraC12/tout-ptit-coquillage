/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 14:54:44 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/20 14:04:09 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"

t_tk_list	*get_if_tk_and(char **str, t_tk_list **token)
{
	if (ft_strncmp(*str, "&&", 2))
		return (NULL);
	(*str) += 2;
	*token = create_token(LX_TK_AND, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_or(char **str, t_tk_list **token)
{
	if (ft_strncmp(*str, "||", 2))
		return (NULL);
	(*str) += 2;
	*token = create_token(LX_TK_OR, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_pipe(char **str, t_tk_list **token)
{
	if (**str != '|')
		return (NULL);
	(*str)++;
	*token = create_token(LX_TK_PIPE, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_left_parenthesis(char **str, t_tk_list **token)
{
	if (**str != '(')
		return (NULL);
	(*str)++;
	*token = create_token(LX_TK_PAR_OPEN, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_right_parenthesis(char **str, t_tk_list **token)
{
	if (**str != ')')
		return (NULL);
	(*str)++;
	*token = create_token(LX_TK_PAR_CLOSE, NULL, 0);
	return (*token);
}
