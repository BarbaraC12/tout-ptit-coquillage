/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 14:54:44 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/25 17:37:56 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"

t_tk_list	*get_tk_litteral(char **str, char *until_chars, int is_quoted)
{
	t_tk_list	*token;
	int			any_other_token;
	int			len;

	any_other_token = !until_chars;
	if (any_other_token)
		until_chars = "$* <>()|\"\'";
	len = 0;
	while ((*str)[len])
	{
		if (ft_strchr(until_chars, (*str)[len])
			|| (any_other_token && !ft_strncmp((*str) + len, "&&", 2)))
			break ;
		len++;
	}
	token = create_token(LX_TK_LITTERAL, *str, len);
	token->is_quoted = is_quoted;
	*str += len;
	return (token);
}

t_tk_list	*get_if_tk_exit_status(char **str, t_tk_list **token)
{
	if (ft_strncmp(*str, "$?", 2))
		return (NULL);
	*str += 2;
	*token = create_token(LX_TK_EXIT_STATUS, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_variable(char **str, t_tk_list **token)
{
	int		len;

	if (**str != '$' || !(ft_isalpha((*str)[1]) || (*str)[1] == '_'))
		return (NULL);
	(*str)++;
	len = 1;
	while (ft_isalnum((*str)[len]) || (*str)[len] == '_')
		len++;
	*token = create_token(LX_TK_VARIABLE, *str, len);
	*str += len;
	return (*token);
}

t_tk_list	*get_if_tk_litteral_dollar(char **str, t_tk_list **token)
{
	if (**str != '$')
		return (NULL);
	*token = create_token(LX_TK_LITTERAL, *str, 1);
	(*str)++;
	return (*token);
}

t_tk_list	*get_if_tk_wildcard(char **str, t_tk_list **token)
{
	if (**str != '*')
		return (NULL);
	while (**str == '*')
		(*str)++;
	*token = create_token(LX_TK_WILDCARD, NULL, 0);
	return (*token);
}
