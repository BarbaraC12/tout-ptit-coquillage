/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokens2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/15 14:54:44 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/03 18:57:01 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"

t_tk_list	*get_if_tk_space(char **str, t_tk_list **token)
{
	if (!ft_isspace(**str))
		return (NULL);
	while (ft_isspace(**str))
		(*str)++;
	*token = create_token(LX_TK_SPACE, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_heredoc(char **str, t_tk_list **token)
{
	if (ft_strncmp(*str, "<<", 2))
		return (NULL);
	(*str) += 2;
	*token = create_token(LX_TK_HEREDOC, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_input(char **str, t_tk_list **token)
{
	if (**str != '<')
		return (NULL);
	(*str)++;
	*token = create_token(LX_TK_INPUT, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_append(char **str, t_tk_list **token)
{
	if (ft_strncmp(*str, ">>", 2))
		return (NULL);
	(*str) += 2;
	*token = create_token(LX_TK_APPEND, NULL, 0);
	return (*token);
}

t_tk_list	*get_if_tk_output(char **str, t_tk_list **token)
{
	if (**str != '>')
		return (NULL);
	(*str)++;
	*token = create_token(LX_TK_OUTPUT, NULL, 0);
	return (*token);
}
