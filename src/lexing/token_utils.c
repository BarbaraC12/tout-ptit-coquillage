/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/21 01:17:59 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/17 03:30:13 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "libft.h"
#include "minishell.h"

/**
 * @brief 			Detaches and frees all the leading tokens
 * 					of type LX_TK_SPACE from the given list.
 *
 * @param[in,out]	tk_list		The list from which to get the token group.
 * @return			The separated group list (NULL if no token group is found).
 */
void	remove_leading_space_tokens(t_tk_list **tk_list)
{
	t_tk_list	*tk_to_free;

	while (*tk_list && (*tk_list)->type == LX_TK_SPACE)
	{
		tk_to_free = *tk_list;
		*tk_list = (*tk_list)->next;
		free(tk_to_free->value);
		free(tk_to_free);
	}
}

/**
 * @brief 			Separate the first token from a given list, and returns it.
 *
 * @param[in,out]	tk_list		The list from which to get the first token.
 * @return			The separated first token (NULL if list is empty).
 */
t_tk_list	*detach_first_token(t_tk_list **tk_list)
{
	t_tk_list	*extracted;

	extracted = *tk_list;
	if (*tk_list)
	{
		*tk_list = (*tk_list)->next;
		extracted->next = NULL;
	}
	return (extracted);
}

/**
 * @brief 			Separate a whole token value group (of types < LX_TK_SPACE)
 * 					from the begining of a given list, and returns it.
 *
 * @param[in,out]	tk_list		The list from which to get the token group.
 * @return			The separated token group list
 * 					(NULL if the list is empty of if the first significative
 * 					token is > LX_TK_SPACE).
 */
t_tk_list	*detach_tk_group(t_tk_list **tk_list)
{
	t_tk_list	*extracted;
	t_tk_list	*search;

	remove_leading_space_tokens(tk_list);
	if (!*tk_list || (*tk_list)->type >= LX_TK_SPACE)
		return (NULL);
	extracted = *tk_list;
	search = *tk_list;
	while (search->next && search->next->type < LX_TK_SPACE)
		search = search->next;
	*tk_list = search->next;
	search->next = NULL;
	remove_leading_space_tokens(tk_list);
	return (extracted);
}

/**
 * @brief					Appends a token list at the end of another,
 * 							separated by a new space token if necessary.
 *
 * @param[in]	tk_list			Pointer to the list to append the other list to.
 * @param[in]	tk_list_to_append	Token list to append.
 * @return		0 on success, 1 on error.
 */
int	append_tk_list(t_tk_list **tk_list, t_tk_list *tk_list_to_append)
{
	t_tk_list	*last_token;

	if (!tk_list_to_append)
		return (0);
	if (!*tk_list)
	{
		*tk_list = tk_list_to_append;
		return (0);
	}
	last_token = *tk_list;
	while (last_token->next)
		last_token = last_token->next;
	if (last_token->type < LX_TK_SPACE && tk_list_to_append->type < LX_TK_SPACE)
	{
		last_token->next = create_token(LX_TK_SPACE, NULL, 0);
		if (!last_token->next)
			return (1);
		last_token = last_token->next;
	}
	last_token->next = tk_list_to_append;
	return (0);
}

/**
 * @brief					Inserts a token list at a specified spot in another,
 * 							adding a new space token after it if necessary, then
 * 							makes the spot point to the next element after the
 * 							inserted bit.
 *
 * @param[in]	spot		Pointer to a list spot to insert the other list at.
 * @param[in]	list		Token list to insert.
 * @return					0 on success, 1 on error.
 */
int	insert_at_spot_and_point_next(t_tk_list ***spot, t_tk_list *list)
{
	t_tk_list	*last_token;

	if (!list)
		return (0);
	last_token = list;
	while (last_token->next)
		last_token = last_token->next;
	if (**spot && (**spot)->type < LX_TK_SPACE
		&& last_token->type < LX_TK_SPACE)
	{
		last_token->next = create_token(LX_TK_SPACE, NULL, 0);
		if (!last_token->next)
			return (1);
		last_token = last_token->next;
	}
	last_token->next = **spot;
	**spot = list;
	*spot = &last_token->next;
	return (0);
}
