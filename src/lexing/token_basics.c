/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   token_basics.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/14 03:33:52 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/20 19:36:37 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"
#include "libft.h"
#include "minishell.h"

/**
 * @brief		Creates a new unlinked token with a given type and value.
 *
 * @param[in]	type	Should be a integer from enum e_token_types.
 * @param[in]	val_src	Source char* to create the token value from (if !NULL).
 * @param[in]	val_len	Length of the source string.
 * @return		Pointer to the mallocated token.
 */
t_tk_list	*create_token(int type, const char *val_src, size_t val_len)
{
	t_tk_list	*token;

	token = malloc(sizeof(t_tk_list));
	if (!token)
		return (ms_ptr_error(NULL, NULL, NULL, NULL));
	token->type = type;
	token->is_quoted = 0;
	token->next = NULL;
	if (val_src)
	{
		token->value = malloc(sizeof(char) * (val_len + 1));
		if (!token->value)
		{
			free(token);
			return (ms_ptr_error(NULL, NULL, NULL, NULL));
		}
		ft_strlcpy(token->value, val_src, (val_len + 1));
	}
	else
		token->value = NULL;
	return (token);
}

/**
 * @brief		Frees a whole token list, given its first token.
 *
 * @param[in]	tk_list	First token of the list.
 * @return		NULL
 */
t_tk_list	*free_token_list(t_tk_list *tk_list)
{
	t_tk_list	*tk_to_free;

	while (tk_list)
	{
		tk_to_free = tk_list;
		tk_list = tk_list->next;
		free(tk_to_free->value);
		free(tk_to_free);
	}
	return (NULL);
}

const char	*get_tk_prefix(int tk_type)
{
	static const char *const	tk_prefix[] = {
	[LX_TK_EXIT_STATUS] = "$?",
	[LX_TK_VARIABLE] = "$",
	[LX_TK_WILDCARD] = "*",
	[LX_TK_LITTERAL] = "",
	[LX_TK_SPACE] = " ",
	[LX_TK_HEREDOC] = "<<",
	[LX_TK_INPUT] = "<",
	[LX_TK_APPEND] = ">>",
	[LX_TK_OUTPUT] = ">",
	[LX_TK_PAR_CLOSE] = ")",
	[LX_TK_PAR_OPEN] = "(",
	[LX_TK_PIPE] = "|",
	[LX_TK_AND] = "&&",
	[LX_TK_OR] = "||"
	};

	return (tk_prefix[tk_type]);
}
