/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 02:01:31 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 23:11:01 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <wait.h>
#include <errno.h>
#include <signal.h>
#include "minishell.h"
#include "builtin.h"
#include "parsing.h"
#include "launcher.h"
#include "env.h"
#include "libft.h"

void	set_subproc(t_env_lst *env)
{
	if (!get_venv(env, "SUBPROC"))
		add_back_env_lst(&env, "SUBPROC=", "SUBPROC", "");
}

static int	subshell_command(t_ast_node *cmd_node, char **argv, t_env_lst *env)
{
	int	wstatus;

	cmd_node->pid = fork();
	if (!cmd_node->pid)
	{
		(set_subproc(env), signal(SIGINT, SIG_DFL), signal(SIGQUIT, SIG_DFL));
		if (dup2(cmd_node->in_fd, STDIN_FILENO) == -1
			|| dup2(cmd_node->out_fd, STDOUT_FILENO) == -1)
			clean_exit(cmd_node, env, ms_put_error(NULL, NULL, NULL, 1));
		cmd_node = free_whole_ast(cmd_node);
		if (is_builtin(argv[0]))
			g_status = run_builtin(argv, env, 1, cmd_node);
		else
			g_status = execve_command(argv, env);
		argv = ft_free_tab(argv);
		clean_exit(cmd_node, env, g_status);
	}
	(close_all_node_fds(cmd_node), argv = ft_free_tab(argv));
	if (cmd_node->pid == -1)
		return (ms_put_error(NULL, NULL, NULL, 1));
	if (is_piped_last(cmd_node))
		return (wait_pipeline(cmd_node, &wstatus), update_g_status(wstatus), 0);
	if (is_piped(cmd_node))
		return (0);
	return (waitpid(cmd_node->pid, &wstatus, 0), update_g_status(wstatus), 0);
}

int	resolve_command(t_ast_node *cmd_node, t_env_lst *env)
{
	char	**argv;

	if (cmd_node->in_fd == -1 || cmd_node->out_fd == -1)
	{
		g_status = 1;
		return (0);
	}
	if (expand_variables_in_token_list(&cmd_node->tk_list, env)
		|| expand_wildcards_in_token_list(&cmd_node->tk_list))
		return (1);
	if (!cmd_node->tk_list)
		return (g_status = 0, 0);
	argv = tk_list_to_argv(cmd_node->tk_list);
	if (!argv)
		return (ms_put_error(NULL, NULL, NULL, 1));
	if (is_piped(cmd_node) || !is_builtin(argv[0]))
		return (subshell_command(cmd_node, argv, env));
	else
		g_status = run_builtin(argv, env, cmd_node->out_fd, cmd_node);
	argv = ft_free_tab(argv);
	return (0);
}
