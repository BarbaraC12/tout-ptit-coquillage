/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tk_list_to_argv.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/17 02:47:59 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/19 17:32:36 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lexing.h"
#include "minishell.h"
#include "libft.h"

static char	**unalloc(size_t argc, char **argv)
{
	while (argc)
		free(argv[--argc]);
	free(argv);
	return (NULL);
}

static size_t	count_litteral_tokens(t_tk_list *tk_list)
{
	size_t		count;

	count = 0;
	while (tk_list)
	{
		if (tk_list->type == LX_TK_LITTERAL)
			count ++;
		tk_list = tk_list->next;
	}
	return (count);
}

char	**tk_list_to_argv(t_tk_list *tk_list)
{
	size_t	argc;
	char	**argv;

	argc = count_litteral_tokens(tk_list);
	argv = malloc(sizeof(char *) * (argc + 1));
	if (!argv)
		return (ms_ptr_error(NULL, NULL, NULL, NULL));
	argv[argc] = NULL;
	argc = 0;
	while (tk_list)
	{
		if (tk_list->type == LX_TK_LITTERAL)
		{
			argv[argc] = ft_strdup(tk_list->value);
			if (!argv[argc])
				return (ms_ptr_error(NULL, NULL, NULL, unalloc(argc, argv)));
			argc++;
		}
		tk_list = tk_list->next;
	}
	return (argv);
}
