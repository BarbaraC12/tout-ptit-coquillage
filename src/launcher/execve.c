/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execve.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 09:53:14 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/27 01:45:44 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <stdbool.h>
#include <errno.h>
#include "minishell.h"
#include "libft.h"
#include "env.h"
/*
static int	is_directory_or_permission_failed(char *path)
{
	struct stat	path_stat;

	stat(path, &path_stat);
	if (S_ISDIR(path_stat.st_mode))
		return (ms_put_error(NULL, path, "Is a directory", 1));
	return (0);
}
*/
static char	*add_path(char *path, char *filename)
{
	size_t	path_len;
	size_t	file_len;
	char	*str;
	bool	addslash;

	path = ft_strtrim(path, " \f\n\r\t\v");
	path_len = ft_strlen(path);
	file_len = ft_strlen(filename);
	addslash = (path_len && path[path_len - 1] != '/');
	str = malloc(sizeof(char) * (path_len + addslash + file_len + 1));
	if (!str)
	{
		free(path);
		return (NULL);
	}
	str[path_len + addslash + file_len] = '\0';
	while (file_len--)
		str[path_len + addslash + file_len] = filename[file_len];
	if (addslash)
		str[path_len] = '/';
	while (path_len--)
		str[path_len] = path[path_len];
	free(path);
	return (str);
}

static int	free_and_error(char **envp, char **paths, char *obj, int ret_val)
{
	ft_free_tab(envp);
	ft_free_tab(paths);
	return (ms_put_error(NULL, obj, NULL, ret_val));
}

static void	run_if_slash(char **argv, char **envp)
{
	struct stat	path_stat;
	char		*errmsg;

	execve(argv[0], argv, envp);
	errmsg = strerror(errno);
	if (errno == ENOENT)
		g_status = 127;
	else if (errno == EACCES)
		g_status = 126;
	if (!stat(argv[0], &path_stat) && S_ISDIR(path_stat.st_mode))
		errmsg = "Is a directory";
	ms_put_error(NULL, argv[0], errmsg, 0);
	ft_free_tab(envp);
}

int	execve_command(char **argv, t_env_lst *env)
{
	char	**envp;
	char	**paths;
	char	**iter;
	char	*path;

	envp = lst_to_tab(env);
	paths = ft_split(get_venv_safe(env, "PATH"), ':');
	if (!envp || !paths)
		return (free_and_error(envp, paths, NULL, 1));
	if (ft_strrchr(argv[0], '/'))
		return (ft_free_tab(paths), run_if_slash(argv, envp), g_status);
	iter = paths;
	while (*iter)
	{
		path = add_path(*(iter++), argv[0]);
		if (!path)
			return (free_and_error(paths, envp, NULL, 1));
		(execve(path, argv, envp), free(path));
		if (errno != ENOENT)
			return (free_and_error(paths, envp, argv[0], 1));
	}
	return (ft_free_tab(envp), ft_free_tab(paths),
		ms_put_error(NULL, argv[0], "Command not found", 127));
}
