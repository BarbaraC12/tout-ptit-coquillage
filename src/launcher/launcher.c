/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launcher.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/17 01:52:42 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 18:21:29 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <readline/readline.h>
#include "env.h"
#include "parsing.h"
#include "minishell.h"
#include "launcher.h"

static int	resolve_separator(t_ast_node *separator_node)
{
	if ((separator_node->tk_list->type == LX_TK_AND && g_status)
		|| (separator_node->tk_list->type == LX_TK_OR && !g_status))
	{
		separator_node->right->parent = NULL;
		separator_node->right = free_whole_ast(separator_node->right);
	}
	return (0);
}

static int	launch_node(t_ast_node *node, t_env_lst *env)
{
	(void)env;
	if (node->tk_list && node->tk_list->type >= LX_TK_HEREDOC
		&& node->tk_list->type <= LX_TK_OUTPUT)
		return (resolve_redirection(node, env));
	else if (node->tk_list && node->tk_list->type >= LX_TK_PIPE)
		return (resolve_separator(node));
	else if (node->tk_list && node->tk_list->type >= LX_TK_PAR_OPEN)
		return (resolve_parenthesis(node, env));
	else
		return (resolve_command(node, env));
	return (0);
}

static void	move_to_node(t_ast_node **node, t_ast_node *new, t_ast_node **last)
{
	*last = *node;
	*node = new;
}

int	launch_ast(t_ast_node *root, t_env_lst *env)
{
	t_ast_node	*node;
	t_ast_node	*lastnode;

	node = root;
	lastnode = NULL;
	while (node)
	{
		if (lastnode == node->parent && node->left)
		{
			move_to_node(&node, node->left, &lastnode);
			continue ;
		}
		if (!lastnode || lastnode != node->right)
		{
			if (launch_node(node, env))
				return (1);
			if (node->right)
			{
				move_to_node(&node, node->right, &lastnode);
				continue ;
			}
		}
		move_to_node(&node, node->parent, &lastnode);
	}
	return (0);
}

void	clean_exit(t_ast_node *ast, t_env_lst *env, int status)
{
	rl_clear_history();
	free_whole_ast(ast);
	clear_env_lst(env);
	exit(status);
}
