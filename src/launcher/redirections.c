/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   redirections.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/11 06:25:25 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 18:33:43 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "launcher.h"
#include "minishell.h"

static int	is_ambiguous_redirect(t_tk_list *tk_list, char *obj)
{
	while (tk_list && tk_list->type != LX_TK_SPACE)
		tk_list = tk_list->next;
	if (tk_list)
		return (ms_put_error(NULL, obj, "ambiguous redirect", 1));
	return (0);
}

static int	open_in(int *fd, t_ast_node *file_node, t_env_lst *env)
{
	char	*filepath;
	char	*obj;

	obj = unexpand_tk_list(file_node->tk_list);
	if (expand_variables_in_token_list(&file_node->tk_list, env)
		|| expand_wildcards_in_token_list(&file_node->tk_list))
		return (1);
	if (is_ambiguous_redirect(file_node->tk_list, obj))
		return (free(obj), *fd = -1, 0);
	free(obj);
	filepath = unexpand_tk_list(file_node->tk_list);
	if (!filepath)
		return (1);
	if (*fd != STDIN_FILENO)
		close (*fd);
	*fd = open(filepath, O_RDONLY);
	if (*fd == -1)
		ms_put_error(NULL, filepath, NULL, 0);
	free(filepath);
	return (0);
}

static int	open_out(int *fd, t_ast_node *file_node, t_env_lst *env, int flags)
{
	char	*filepath;
	char	*obj;

	obj = unexpand_tk_list(file_node->tk_list);
	if (expand_variables_in_token_list(&file_node->tk_list, env)
		|| expand_wildcards_in_token_list(&file_node->tk_list))
		return (1);
	if (is_ambiguous_redirect(file_node->tk_list, obj))
		return (free(obj), *fd = -1, 0);
	free(obj);
	filepath = unexpand_tk_list(file_node->tk_list);
	if (!filepath)
		return (1);
	if (*fd != STDOUT_FILENO)
		close (*fd);
	*fd = open(filepath, flags, 0666);
	if (*fd == -1)
		ms_put_error(NULL, filepath, NULL, 0);
	free(filepath);
	return (0);
}

static void	propagate_fds(t_ast_node *redir_node)
{
	if (redir_node->parent->in_fd != -1)
	{
		if (redir_node->parent->in_fd > 1
			&& close(redir_node->parent->in_fd))
			ms_put_error(NULL, NULL, NULL, 0);
		redir_node->parent->in_fd = redir_node->in_fd;
		redir_node->in_fd = 0;
	}
	if (redir_node->parent->out_fd != -1)
	{
		if (redir_node->parent->out_fd > 1
			&& close(redir_node->parent->out_fd))
			ms_put_error(NULL, NULL, NULL, 0);
		redir_node->parent->out_fd = redir_node->out_fd;
		redir_node->out_fd = 1;
	}
}

int	resolve_redirection(t_ast_node *redir_node, t_env_lst *env)
{
	int	err;

	err = 0;
	if (redir_node->in_fd != -1 && redir_node->out_fd != -1)
	{
		if (redir_node->tk_list->type == LX_TK_HEREDOC)
			err = expand_heredoc(
					&redir_node->in_fd, redir_node->right, env);
		else if (redir_node->tk_list->type == LX_TK_INPUT)
			err = open_in(&redir_node->in_fd, redir_node->right, env);
		else if (redir_node->tk_list->type == LX_TK_APPEND)
			err = open_out(&redir_node->out_fd, redir_node->right, env,
					O_CREAT | O_WRONLY | O_APPEND);
		else if (redir_node->tk_list->type == LX_TK_OUTPUT)
			err = open_out(&redir_node->out_fd, redir_node->right, env,
					O_CREAT | O_WRONLY | O_TRUNC);
		else
			err = 1;
	}
	if (!err)
	{
		redir_node->right = free_ast_node(redir_node->right);
		propagate_fds(redir_node);
	}
	return (err);
}
