/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipes.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 06:46:02 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 02:43:14 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <wait.h>
#include "minishell.h"
#include "parsing.h"

int	is_piped(t_ast_node *node)
{
	return (node->parent && (node->parent->tk_list->type == LX_TK_PIPE
			|| (node->parent->parent
				&& node->parent->parent->tk_list->type == LX_TK_PIPE)));
}

int	is_piped_last(t_ast_node *node)
{
	return (node->parent && node->parent->tk_list->type == LX_TK_PIPE
		&& node->parent->right == node
		&& (!node->parent->parent
			|| node->parent->parent->tk_list->type != LX_TK_PIPE));
}

void	wait_pipeline(t_ast_node *last_piped_node, int *wstatus)
{
	t_ast_node	*pipe_separator;

	pipe_separator = last_piped_node->parent;
	while (pipe_separator->left
		&& pipe_separator->left->tk_list->type == LX_TK_PIPE)
	{
		pipe_separator = pipe_separator->left;
		if (pipe_separator->right->pid != -1)
			waitpid(pipe_separator->right->pid, NULL, 0);
	}
	if (pipe_separator->left->pid != -1)
		waitpid(pipe_separator->left->pid, NULL, 0);
	if (last_piped_node->pid != -1)
		waitpid(last_piped_node->pid, wstatus, 0);
}

void	close_all_node_fds(t_ast_node *node)
{
	if (node->in_fd != -1 && node->in_fd != STDIN_FILENO
		&& close(node->in_fd))
		ms_put_error(NULL, NULL, NULL, 0);
	if (node->out_fd != -1 && node->out_fd != STDOUT_FILENO
		&& close(node->out_fd))
		ms_put_error(NULL, NULL, NULL, 0);
	node->in_fd = -1;
	node->out_fd = -1;
}
