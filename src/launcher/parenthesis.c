/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parenthesis.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 02:15:27 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 13:16:23 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <wait.h>
#include "libft.h"
#include "minishell.h"
#include "parsing.h"
#include "launcher.h"
#include "env.h"

static void	run_subshell_ast(t_ast_node *parenthesis_node, t_env_lst *env)
{
	t_ast_node	*subshell_root;

	set_subproc(env);
	if (parenthesis_node->in_fd == -1 || parenthesis_node->out_fd == -1)
		clean_exit(parenthesis_node, env, 1);
	if (dup2(parenthesis_node->in_fd, STDIN_FILENO) == -1
		|| dup2(parenthesis_node->out_fd, STDOUT_FILENO) == -1)
		clean_exit(parenthesis_node, env, ms_put_error(NULL, NULL, NULL, 1));
	close_all_node_fds(parenthesis_node);
	subshell_root = parenthesis_node->right;
	parenthesis_node->right = NULL;
	subshell_root->parent = NULL;
	parenthesis_node = free_whole_ast(parenthesis_node);
	launch_ast(subshell_root, env);
	clean_exit(subshell_root, env, g_status);
}

int	resolve_parenthesis(t_ast_node *parenthesis_node, t_env_lst *env)
{
	int			wstatus;

	parenthesis_node->pid = fork();
	if (!parenthesis_node->pid)
		run_subshell_ast(parenthesis_node, env);
	if (parenthesis_node->pid == -1)
		return (ms_put_error(NULL, NULL, NULL, 1));
	close_all_node_fds(parenthesis_node);
	parenthesis_node->right->parent = NULL;
	parenthesis_node->right = free_whole_ast(parenthesis_node->right);
	if (is_piped_last(parenthesis_node))
		wait_pipeline(parenthesis_node, &wstatus);
	else if (is_piped(parenthesis_node))
		return (0);
	else
		waitpid(parenthesis_node->pid, &wstatus, 0);
	update_g_status(wstatus);
	return (0);
}
