/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_heredoc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/25 16:46:49 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 22:40:32 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include "parsing.h"

static t_tk_list	*microlexing(char *str)
{
	t_tk_list	*root;
	t_tk_list	**spot;

	root = NULL;
	spot = &root;
	while (*str)
	{
		if (!get_if_tk_exit_status(&str, spot)
			&& !get_if_tk_variable(&str, spot)
			&& !get_if_tk_litteral_dollar(&str, spot))
			*spot = get_tk_litteral(&str, "$", 1);
		if (!*spot)
			return (free_token_list(root), NULL);
		(*spot)->is_quoted = 2;
		spot = &(*spot)->next;
	}
	return (root);
}

static int	is_there_quotes(t_tk_list *tk_list)
{
	while (tk_list)
	{
		if (tk_list->is_quoted)
			return (1);
		tk_list = tk_list->next;
	}
	return (0);
}

static int	expand_line(char **line, t_env_lst *env)
{
	t_tk_list	*line_tks;
	char		*newline;

	line_tks = microlexing(*line);
	if (expand_variables_in_token_list(&line_tks, env))
		return (free_token_list(line_tks), 1);
	newline = unexpand_tk_list(line_tks);
	free_token_list(line_tks);
	if (!newline)
		return (1);
	free(*line);
	*line = newline;
	return (0);
}

static int	close_fds(int fd1, int fd2)
{
	if (close(fd1))
		return (close(fd2), ms_put_error(NULL, NULL, NULL, 1));
	if (close(fd2))
		return (ms_put_error(NULL, NULL, NULL, 1));
	return (0);
}

int	expand_heredoc(int *fd, t_ast_node *heredoc_cond_node, t_env_lst *env)
{
	char		*line;
	int			pipefd[2];

	if (*fd == -1 || is_there_quotes(heredoc_cond_node->tk_list))
		return (0);
	if (pipe(pipefd))
		return (ms_put_error(NULL, NULL, NULL, 1));
	line = ft_gnl(*fd);
	while (line)
	{
		if (expand_line(&line, env))
			break ;
		if (write(pipefd[1], line, ft_strlen(line)) == -1)
		{
			ms_put_error(NULL, NULL, NULL, 0);
			break ;
		}
		free(line);
		line = ft_gnl(*fd);
	}
	free(line);
	if (close_fds(*fd, pipefd[1]))
		return (close (pipefd[0]), *fd = -1, 1);
	*fd = pipefd[0];
	return (0);
}
