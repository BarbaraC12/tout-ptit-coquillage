/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_print.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:53:19 by bcano             #+#    #+#             */
/*   Updated: 2022/01/28 17:10:22 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"
#include "stdio.h"

void	print_env_node(t_env_node const *const node)
{
	if (node->value)
		printf("%s=%s\n", node->name, node->value);
}

void	print_env_lst(t_env_lst const *lst)
{
	t_env_node	*current;

	current = lst->head;
	while (current != NULL)
	{
		print_env_node(current);
		current = current->next;
	}
}
