/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_edit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:50:10 by bcano             #+#    #+#             */
/*   Updated: 2022/01/28 17:26:11 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"
#include "libft.h"
#include "minishell.h"

int	env_replace_value(char *name, t_env_lst *lst, char *new_value)
{
	t_env_node	*current;
	t_env_node	*next;

	if (name == NULL)
		return (0);
	current = lst->head;
	while (current != NULL)
	{
		next = current->next;
		if (!ft_strcmp(current->name, name))
		{
			free(current->env);
			current->env = ft_strjoin3(name, "=", new_value);
			free(current->value);
			current->value = ft_strdup(new_value);
			return (0);
		}
		current = next;
	}
	return (1);
}

int	set_new_pwd(t_env_lst *lst, char *new_value, char *last_value)
{
	int		pwd;
	int		old;
	char	*env;

	pwd = env_replace_value("PWD", lst, new_value);
	free(new_value);
	if (pwd == 1)
	{
		env = ft_strjoin3("PWD", "=", new_value);
		pwd = add_back_env_lst(&lst, env, "PWD", new_value);
		free(env);
		if (pwd)
			return (free(last_value), EXIT_FAILURE);
	}
	old = env_replace_value("OLDPWD", lst, last_value);
	if (old)
	{
		env = ft_strjoin3("OLDPWD", "=", last_value);
		old = add_back_env_lst(&lst, env, "OLDPWD", last_value);
		free(env);
		if (old)
			return (free(last_value), EXIT_FAILURE);
	}
	return (free(last_value), EXIT_SUCCESS);
}
