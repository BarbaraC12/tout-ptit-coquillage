/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:46:07 by bcano             #+#    #+#             */
/*   Updated: 2022/01/18 19:56:28 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"
#include "libft.h"

/**
 * @brief		Function who translate list to table
 *
 * @param		node the list chained of envp
 * @return		A table who point to lst elements 
 */
char	**lst_to_tab(t_env_lst *lst)
{
	char		**tab;
	t_env_node	*current;
	size_t		i;

	i = 0;
	tab = NULL;
	tab = malloc(sizeof(char *) * (lst->lst_size + 1));
	tab[lst->lst_size] = NULL;
	current = lst->head;
	while (current != NULL)
	{
		tab[i] = ft_strdup((char *)current->env);
		if (!tab[i])
			break ;
		current = current->next;
		i++;
	}
	if (i != lst->lst_size)
	{
		while (i)
			free(tab[--i]);
		return (free(tab), NULL);
	}
	return (tab);
}
