/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_get_params.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:44:41 by bcano             #+#    #+#             */
/*   Updated: 2022/01/27 00:55:51 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"
#include "libft.h"

/**
 * @brief		Recover only the name before the 1st '=' in a string
 * 
 * @param		str The string to be process
 * @return		Name found if exist else NULL 
 */
char	*get_name(char *const str)
{
	char	*name;
	size_t	i;
	size_t	len;

	len = 0;
	name = 0;
	if (!str || str[len] == '=')
		return (NULL);
	while (str[len])
	{
		if (str[len] == '=')
			break ;
		len++;
	}
	name = (char *)malloc(sizeof(char) * (len + 1));
	if (!name)
		return (NULL);
	i = 0;
	while (i < len)
	{
		name[i] = str[i];
		i++;
	}
	name[i] = '\0';
	return (name);
}

/**
 * @brief		Recover only the value after the 1st '=' in a string
 * 
 * @param		str The string to be process
 * @return		Value found if exist else an empty line
 */
char	*get_value(char const *str)
{
	char	*value;
	size_t	j;
	size_t	i;

	j = 0;
	while (str[j] && str[j] != '=')
		j++;
	if (str[j++] != '=')
		return (NULL);
	value = (char *)malloc(sizeof(char) * (ft_strlen(str) - j + 1));
	if (!value)
		return (NULL);
	i = 0;
	while (str[j])
		value[i++] = str[j++];
	value[i] = '\0';
	return (value);
}

/**
 * @brief		Get size of tab
 *
 * @param		tab the table to 
 * @return		number of element
 */
size_t	size_tab(char **tab)
{
	size_t	nb;

	nb = 0;
	while (tab[nb])
		nb++;
	return (nb);
}

/**
 * @brief		Get a value of variable from a name in env (chained list)
 *
 * @param		lst The chained list env
 * @param		name The name searched in env
 * @return		char *value return NULL if can't find name
 */
char	*get_venv(t_env_lst const *lst, const char *name)
{
	t_env_node	*current;

	current = lst->head;
	while (current)
	{
		if (!ft_strcmp(current->name, name))
		{
			if (!current->value)
				return ("");
			return (current->value);
		}
		current = current->next;
	}
	return (NULL);
}

/**
 * @brief		Get a value of variable from a name in env (chained list)
 * 				This function is util for the lexing
 *
 * @param		lst The chained list env
 * @param		name The name searched in env
 * @return		char *value return empty array if can't find name
 */
char	*get_venv_safe(t_env_lst const *lst, const char *name)
{
	t_env_node	*current;

	current = lst->head;
	while (current)
	{
		if (!ft_strcmp(current->name, name))
		{
			if (current->value)
				return (current->value);
			return ("");
		}
		current = current->next;
	}
	return ("");
}
