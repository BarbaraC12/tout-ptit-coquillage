/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_add.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:24:42 by bcano             #+#    #+#             */
/*   Updated: 2022/01/27 00:44:18 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"
#include "libft.h"
#include "minishell.h"

#define NAME "PATH"
#define PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
/**
 * @brief		Create and alloc new node at the init
 *
 * @param		name The name of node to create. Can't be NULL
 * @param		value The value of node that will be create.
 * @return		If fail NULL else the new node.
 */
t_env_node	*new_env_node(char *env, char *name, char *value)
{
	t_env_node	*node;

	node = (t_env_node *)malloc(sizeof(t_env_node));
	if (!node)
		return (NULL);
	if (env && name)
	{
		node->env = ft_strdup(env);
		if (!node->env)
			return (NULL);
		node->name = ft_strdup(name);
		if (!node->name)
			return (free(node->env), NULL);
		if (value)
		{
			node->value = ft_strdup(value);
			if (!node->value)
				return (free(node->env), free(node->name), NULL);
		}
		else
			node->value = NULL;
	}
	node->prev = NULL;
	node->next = NULL;
	return (node);
}

/**
 * @brief		Set address to new node at Tail list
 *
 * @param		lst The chained list 
 * @param		node The node to add att Tail
 * @return		Nothing
 */
void	push_back_env_lst(t_env_lst *const lst, t_env_node *const node)
{
	if (lst->lst_size == 0)
	{
		lst->head = node;
		lst->tail = node;
	}
	else
	{
		node->prev = lst->tail;
		lst->tail->next = node;
		lst->tail = node;
	}
	lst->lst_size++;
}

/**
 * @brief		Create and add new node at the init
 *
 * @param		lst The chained list to add node
 * @param		name The name of node to create. Can't be NULL
 * @param		value The value of node that will be create.
 * @return		If fail EXIT_FAILURE else EXIT_SUCCESS
 */
int	add_back_env_lst(t_env_lst **const lst, char *env, char *name, char *value)
{
	t_env_node*const	node = new_env_node(env, name, value);

	if (!node)
		return (EXIT_FAILURE);
	push_back_env_lst(*lst, node);
	return (EXIT_SUCCESS);
}

int	set_env(char **envp, t_env_lst *lst)
{
	char		*name;
	char		*value;
	int			i;

	i = 0;
	name = NULL;
	value = NULL;
	while (envp[i])
	{
		name = get_name(envp[i]);
		if (name == NULL)
			return (EXIT_FAILURE);
		value = get_value(envp[i]);
		if (!value)
			return (free(name), EXIT_FAILURE);
		if (add_back_env_lst(&lst, envp[i], name, value) != EXIT_SUCCESS)
		{
			clear_env_lst(lst);
			return (free(name), free(value), EXIT_FAILURE);
		}
		free(name);
		free(value);
		i++;
	}
	return (EXIT_SUCCESS);
}
