/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chained_delete.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 19:38:58 by bcano             #+#    #+#             */
/*   Updated: 2022/01/27 00:45:41 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "env.h"

void	delone_env_lst(t_env_lst *const lst, t_env_node *node)
{
	t_env_node	*current;

	current = lst->head;
	while (current != NULL)
	{
		if (current == node)
		{
			if (current->next != NULL)
				current->next->prev = current->prev;
			if (current->prev != NULL)
				current->prev->next = current->next;
			if (current == lst->head)
				lst->head = current->next;
			if (current == lst->tail)
				lst->tail = current->prev;
			lst->lst_size--;
			free(current->env);
			free(current->name);
			free(current->value);
			free(current);
			break ;
		}
		current = current->next;
	}	
}

void	error_clear_env_lst(t_env_lst **const lst)
{
	t_env_node	*current;
	t_env_node	*next;

	current = (*lst)->head;
	while (current != NULL)
	{
		next = current->next;
		delone_env_lst(*lst, current);
		current = next;
	}
}

void	clear_env_lst(t_env_lst *const lst)
{
	t_env_node	*current;
	t_env_node	*next;

	current = lst->head;
	while (current != NULL)
	{
		next = current->next;
		delone_env_lst(lst, current);
		current = next;
	}
}
