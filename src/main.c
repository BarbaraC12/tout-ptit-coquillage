/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 14:05:44 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/28 20:08:55 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <signal.h>
#include <readline/readline.h>
#include <readline/history.h>
#include "minishell.h"
#include "libft.h"
#include "env.h"
#include "lexing.h"
#include "parsing.h"
#include "launcher.h"
#include "builtin.h"

int	g_status = 0;

static int	minishell_init(int argc, char **argv, char **envp, t_env_lst *env)
{
	(void)argc;
	(void)argv;
	signal(SIGQUIT, SIG_IGN);
	env->head = NULL;
	env->tail = NULL;
	env->lst_size = 0;
	if (set_env(envp, env))
		return (1);
	return (0);
}

static int	is_empty_line(char *line)
{
	while (ft_isspace(*line))
		line++;
	return (!*line);
}

static void	loop_core(char *line, t_env_lst *env)
{
	t_tk_list	*tk_list;
	t_ast_node	*ast_root;

	if (is_empty_line(line))
		return ;
	add_history(line);
	tk_list = build_tokens_list(line);
	free(line);
	remove_leading_space_tokens(&tk_list);
	ast_root = build_ast(&tk_list);
	tk_list = free_token_list(tk_list);
	if (g_status != 128)
	{
		signal(SIGINT, &executive_sighandler);
		signal(SIGQUIT, &sigquit_handler);
		launch_ast(ast_root, env);
		signal(SIGQUIT, SIG_IGN);
	}
	else
		g_status = 130;
	ast_root = free_whole_ast(ast_root);
}

int	main(int argc, char **argv, char **envp)
{
	t_env_lst	env;
	char		*line;

	if (minishell_init(argc, argv, envp, &env))
		return (1);
	signal(SIGINT, &interactive_sighandler);
	line = readline("➤ \033[0;34m"MSH" \033[0m$ ");
	while (line)
	{
		loop_core(line, &env);
		signal(SIGINT, &interactive_sighandler);
		if (g_status)
			line = readline("\033[0;31m➤ \033[0;34m"MSH" \033[0m$ ");
		else
			line = readline("\033[0;32m➤ \033[0;34m"MSH" \033[0m$ ");
	}
	builtin_exit(ft_split("exit", ' '), &env, NULL);
	return (get_g_status());
}
