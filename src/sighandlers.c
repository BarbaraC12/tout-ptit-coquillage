/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sighandlers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 07:21:30 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/28 20:09:29 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <readline/readline.h>
#include "minishell.h"

void	interactive_sighandler(int sig)
{
	(void)sig;
	g_status = 130;
	if (g_status)
		printf("\n\033[0;31m");
	else
		printf("\n\033[0;32m");
	rl_on_new_line();
	rl_replace_line("", 0);
	rl_redisplay();
}

void	executive_sighandler(int sig)
{
	(void)sig;
	printf("\n");
}

void	heredoc_sighandler(int sig)
{
	(void)sig;
	close (STDIN_FILENO);
	g_status = 128;
	printf("\n");
}

void	sigquit_handler(int sig)
{
	(void)sig;
	printf("Quit (core dumped)\n");
	g_status = 131;
}
