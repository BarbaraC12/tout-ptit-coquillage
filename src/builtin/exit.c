/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:12:57 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 13:32:06 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "minishell.h"
#include "parsing.h"
#include "env.h"
#include "libft.h"
#include <stdio.h>
#include <readline/readline.h>

#define ERR_ARG	"too many arguments"
#define ERR_NUM	"numeric arguement required"
#define EXIT	"exit\n"

static int	ft_strdigit(char *str)
{
	long long int	status;

	status = -1;
	if (ft_atoll_err(str, &status))
		return (ms_put_error(MSH": exit", str, ERR_NUM, 2));
	status &= 255ULL;
	return ((int)status);
}

int	builtin_exit(char **argv, t_env_lst *lst, t_ast_node *ast)
{
	if (!get_venv(lst, "SUBPROC"))
		write(2, EXIT, ft_strlen(EXIT));
	if (argv[1] && argv[2])
		return (ms_put_error(MSH, "exit", ERR_ARG, 2), 2);
	else
	{
		free_whole_ast(ast);
		clear_env_lst(lst);
		rl_clear_history();
		if (argv[1] != NULL)
			g_status = ft_strdigit(argv[1]);
		ft_free_tab(argv);
		exit(g_status);
	}
}
