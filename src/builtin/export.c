/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   export.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:13:41 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 20:45:58 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "minishell.h"
#include "libft.h"
#include <stdio.h>
#include "env.h"

#define Y	"\033[0;33m"
#define G	"\033[0;90m"
#define R	"\033[0m \n"
#define INV_ID	"not a valid identifier"

#define ERR_ADD	42

static int	export_unspecified(void)
{
	printf("%s─────╔═╗─────╔╦═╦╗──╔╗ ╔╗──╔╗──────╔╗    ▄   ▄    \n", G);
	printf("╔╦╦═╦╣═╬═╦═╦═╬╣═╬╬═╦╝║ ║╚╦═╣╚╦═╦═╦═╬╬═╦╦╗█▀█▀█    \n");
	printf("║║║║║╠═║╬║╩╣═╣║╔╣║╩╣╬║ ║╬║╩╣║║╬╚╗║╔╣║╬║╔╝█▄█▄█    \n");
	printf("╚═╩╩═╩═╣╔╩═╩═╩╩╝╚╩═╩═╝ ╚═╩═╩╩╩══╩═╝╚╩═╩╝ ─███──▄▄ \n");
	printf("───────╚╝                                ─████▐█─█\n");
	printf("      %sWhen no arguments are given,%s       ─████───█\n", Y, G);
	printf("      %sthe results are unspecified.%s       ─▀▀▀▀▀▀▀ %s", Y, G, R);
	return (0);
}

static int	invalid_identifier(char *arg)
{
	int	i;

	i = 0;
	if (!ft_isalpha(arg[i]) && arg[i++] != '_')
		return (1);
	while (ft_isalnum(arg[i]) || arg[i] == '_')
		i++;
	return (!!arg[i] && arg[i] != '=');
}

int	builtin_export(char **argv, t_env_lst *lst)
{
	char	*name;
	char	*val;

	name = NULL;
	val = NULL;
	if (*argv == NULL)
		return (export_unspecified());
	else
	{
		while (*argv != NULL)
		{
			if (invalid_identifier(*argv))
				return (ms_put_error(MSH": export", *argv, INV_ID, 1));
			name = get_name(*argv);
			if (!name)
				return (free(name), ERR_ADD);
			val = get_value(*argv);
			if (get_venv(lst, name) && !env_replace_value(name, lst, val))
				return (free(val), free(name), 0);
			else if (add_back_env_lst(&lst, *argv, name, val) == EXIT_SUCCESS)
				return (free(val), free(name), 0);
			argv++;
		}
	}
	return (0);
}
