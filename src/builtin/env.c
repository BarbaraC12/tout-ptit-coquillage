/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:12:36 by bcano             #+#    #+#             */
/*   Updated: 2022/01/19 03:04:04 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "minishell.h"
#include "env.h"
#include <stdio.h>

#define ARGS	"too many arguments"

int	builtin_env(char **arg, t_env_lst *lst)
{
	if (arg[0] != NULL)
		return (ms_put_error("env", NULL, ARGS, 127));
	else
		print_env_lst(lst);
	return (0);
}
