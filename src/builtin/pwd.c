/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pwd.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 21:28:19 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 18:46:10 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "builtin.h"
#include "libft.h"
#include "env.h"
#include <errno.h>
#include <unistd.h>
#include <stdio.h>

#define ERR_ACCES	"Permission denied"

int	builtin_pwd(char **argv, int out)
{
	char	*pwd;
	int		err;

	err = 0;
	(void)argv;
	pwd = getcwd(NULL, 0);
	if (pwd == NULL)
	{
		err = ms_put_error("pwd", NULL, ERR_ACCES, EACCES);
		return (err);
	}	
	write(out, pwd, ft_strlen(pwd));
	write(out, "\n", 1);
	free(pwd);
	return (err);
}
