/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unset.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:14:02 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 00:51:00 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "builtin.h"
#include "env.h"
#include "libft.h"

int	builtin_unset(char **argv, t_env_lst *lst)
{
	t_env_node	*current;
	t_env_node	*next;

	if (*argv != NULL)
	{
		while (*argv != NULL)
		{
			current = lst->head;
			while (current != NULL)
			{
				next = current->next;
				if (!ft_strcmp(current->name, *argv))
					delone_env_lst(lst, current);
				current = next;
			}
			argv++;
		}
	}
	return (0);
}
