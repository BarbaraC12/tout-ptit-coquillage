/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 15:12:01 by bcano             #+#    #+#             */
/*   Updated: 2022/01/28 17:28:22 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "env.h"
#include "minishell.h"
#include "libft.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>

static int	no_home(char **home, char **argv)
{
	int	i;

	i = 0;
	(void)home;
	if (argv[i++] == NULL)
		printf("bash: cd: HOME no set\n");
	else if (argv[i] && argv[i +1])
		printf("bash: cd: too many arguments\n");
	else
		return (0);
	return (1);
}

static int	no_dir(char *arg, char **pwd)
{
	printf("cd: no such file or directory: %s\n", arg);
	free(*pwd);
	return (1);
}

static int	go_dir(t_env_lst *lst, char *new, char *last, bool flag)
{
	if (chdir(new) == -1)
		return (1);
	if (flag == 1)
		printf("~%s\n", new);
	set_new_pwd(lst, getcwd(NULL, 0), last);
	return (0);
}

static int	init_struct_cd(t_cd_utl	*init, t_env_lst *lst, char **argv)
{
	init->home = get_venv(lst, "HOME");
	init->cdpath = NULL;
	init->cd = NULL;
	if (init->home == NULL && no_home(&init->home, argv))
		return (1);
	else if (argv[0] != NULL && argv[1] != NULL)
		return ((init->home = NULL),
			ms_put_error(MSH, "cd", "too many arguments", 1));
	else
	{
		init->pwd = getcwd(NULL, 0);
		init->oldpwd = get_venv(lst, "OLDPWD");
		if (!init->pwd)
			return (1);
		if (!(*argv) && init->pwd)
			return (go_dir(lst, init->home, init->pwd, 0));
	}
	return (-1);
}

int	builtin_cd(char **argv, t_env_lst *lst)
{
	t_cd_utl	struct_cd;
	int			err;

	err = init_struct_cd(&struct_cd, lst, argv);
	if (err != -1)
		return (err);
	struct_cd.cdpath = get_venv(lst, "CDPATH");
	if (!ft_strcmp(argv[0], "-"))
		return (go_dir(lst, struct_cd.oldpwd, struct_cd.pwd, 1));
	struct_cd.oldpwd = struct_cd.pwd;
	if (struct_cd.cdpath)
	{
		struct_cd.cd = path_name(struct_cd.cdpath, *argv);
		if (go_dir(lst, struct_cd.cd, struct_cd.pwd, 0) == 0)
		{
			printf("%s\n", struct_cd.cd);
			return (free(struct_cd.cd), 0);
		}
		free(struct_cd.cd);
	}
	if (chdir(*argv) != 0)
		return (no_dir(*argv, &struct_cd.pwd));
	struct_cd.cd = path_name(struct_cd.oldpwd, *argv);
	return (set_new_pwd(lst, getcwd(NULL, 0), struct_cd.pwd),
		free(struct_cd.cd), 1);
}
