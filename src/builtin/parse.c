/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/14 16:07:42 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 06:34:16 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "parsing.h"
#include "libft.h"

/**
 * @brief		Check if argument passed is a builtin of not
 *
 * @param		cmd list of arguments
 * @return		0 if not builtin, else a number between 1 and 7
 */
int	is_builtin(char *cmd)
{
	int	ret;

	ret = NOT_BUILTIN;
	if (!ft_strcmp(cmd, "exit"))
		ret = BUILTIN_EXIT;
	if (!ft_strcmp(cmd, "pwd"))
		ret = BUILTIN_PWD;
	if (!ft_strcmp(cmd, "cd"))
		ret = BUILTIN_CD;
	if (!ft_strcmp(cmd, "echo"))
		ret = BUILTIN_ECHO;
	if (!ft_strcmp(cmd, "export"))
		ret = BUILTIN_EXPORT;
	if (!ft_strcmp(cmd, "unset"))
		ret = BUILTIN_UNSET;
	if (!ft_strcmp(cmd, "env"))
		ret = BUILTIN_ENV;
	return (ret);
}

int	run_builtin(char **argv, t_env_lst *lst, int out, t_ast_node *ast)
{
	int	status;

	status = is_builtin(*argv);
	if (status == BUILTIN_EXIT)
		return (builtin_exit(argv, lst, ast));
	else if (status == BUILTIN_PWD)
		return (builtin_pwd(++argv, out));
	else if (status == BUILTIN_CD)
		return (builtin_cd(++argv, lst));
	else if (status == BUILTIN_ECHO)
		return (builtin_echo(++argv, out));
	else if (status == BUILTIN_EXPORT)
		return (builtin_export(++argv, lst));
	else if (status == BUILTIN_UNSET)
		return (builtin_unset(++argv, lst));
	else if (status == BUILTIN_ENV)
		return (builtin_env(++argv, lst));
	return (0);
}
