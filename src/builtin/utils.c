/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/16 16:22:37 by bcano             #+#    #+#             */
/*   Updated: 2022/01/26 13:19:56 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "libft.h"
#include "env.h"
#include <stdlib.h>
#include <stdbool.h>

static char	*join_tab(char **tab, char *del, size_t i)
{
	int		first;
	char	*str;
	char	*tmp;
	size_t	size;

	size = size_tab(tab);
	first = 0;
	if (size == 0 || (size == 1 && !ft_strcmp(tab[i], "(null)")))
		str = ft_strdup(del);
	while (i < size)
	{
		if (tab[i] && ft_strcmp(tab[i], "(null)") && !first)
			str = ft_strjoin(del, tab[i++]);
		if (tab[i] && ft_strcmp(tab[i], "(null)") && ++first)
		{
			tmp = ft_strjoin(str, del);
			free(str);
			str = ft_strjoin(tmp, tab[i]);
			free(tmp);
		}
		i++;
	}
	return (str);
}

char	*real_path(char *path, char *dir)
{
	char	**tab;
	char	*cd;
	size_t	i;

	if (path != NULL)
		tab = ft_split(path, '/');
	else
		tab = ft_split(dir, '/');
	i = size_tab(tab);
	while (i >= 0 && i--)
	{
		if (!ft_strcmp(tab[i], "..") || !ft_strcmp(tab[i], "."))
		{
			if (!ft_strcmp(tab[i], "..") && i - 1 >= 0)
			{
				free(tab[i - 1]);
				tab[i - 1] = ft_strdup("(null)");
			}
			free(tab[i]);
			tab[i] = ft_strdup("(null)");
		}
	}
	return ((cd = join_tab(tab, "/", ++i)), (tab = ft_free_tab(tab)), cd);
}

char	*path_name(char *cdpath, char *dir)
{
	char	*cdpath_name;

	if (dir[0] == '/')
	{
		if (ft_strlen(dir) < 1)
			cdpath_name = ft_strdup("/");
		else
			cdpath_name = real_path(NULL, dir);
	}
	else
		cdpath_name = ft_strjoin3(cdpath, "/", dir);
	if (!cdpath_name)
		return (NULL);
	return (cdpath_name);
}
