/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 17:45:19 by bcano             #+#    #+#             */
/*   Updated: 2022/01/19 03:23:45 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "builtin.h"
#include "minishell.h"
#include "libft.h"
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>

static int	isflag_n(char *arg)
{
	if (ft_strncmp(arg, "-n", 2))
		return (0);
	arg += 2;
	while (*arg == 'n')
		arg++;
	return (!*arg);
}	

static void	write_arg(char **argv, int fd_out)
{
	write(fd_out, *argv, ft_strlen(*argv));
	if (argv[1] != NULL)
		write(fd_out, " ", 1);
}

int	builtin_echo(char **argv, int out)
{
	bool	nflag;

	nflag = 0;
	if (*argv)
		nflag = isflag_n(*argv);
	while (*argv && isflag_n(*argv))
		argv++;
	while (*argv != NULL)
	{
		write_arg(argv, out);
		argv++;
	}
	if (nflag == 0)
		write(out, "\n", 1);
	return (0);
}
