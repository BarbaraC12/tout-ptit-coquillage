/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/23 03:57:51 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 03:43:10 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <linux/limits.h>
#include "minishell.h"
#include "parsing.h"
#include "libft.h"

int	get_g_status(void)
{
	if (g_status < 0)
		return (-(g_status + 1));
	return (g_status);
}

void	update_g_status(int wstatus)
{
	if (WIFEXITED(wstatus))
		g_status = WEXITSTATUS(wstatus);
	else if (WIFSIGNALED(wstatus))
		g_status = 128 + WTERMSIG(wstatus);
	else
		ms_put_error(NULL, "subshell", "NOT EXITED NOR SIGNALED", 0);
}

int	ms_put_error(char *path, char *obj, char *msg, int ret_val)
{
	static char	buf[PATH_MAX * 3 + 5];
	char		*basename;
	size_t		len;

	if (path)
		basename = ft_basename(path);
	else
		basename = "minishell";
	len = 0;
	len += ft_strlcpy(buf + len, basename, PATH_MAX + 1);
	len += ft_strlcpy(buf + len, ": ", 3);
	if (obj)
	{
		len += ft_strlcpy(buf + len, obj, PATH_MAX + 1);
		len += ft_strlcpy(buf + len, ": ", 3);
	}
	if (msg)
		len += ft_strlcpy(buf + len, msg, PATH_MAX + 1);
	else
		len += ft_strlcpy(buf + len, strerror(errno), PATH_MAX + 1);
	len += ft_strlcpy(buf + len, "\n", 2);
	if (write(STDERR_FILENO, buf, len))
		NULL;
	return (ret_val);
}

void	*ms_ptr_error(char *path, char *obj, char *msg, void *ret_ptr)
{
	ms_put_error(path, obj, msg, 0);
	return (ret_ptr);
}
