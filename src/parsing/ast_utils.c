/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ast_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/27 14:55:03 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/19 07:35:40 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "parsing.h"
#include "minishell.h"

/**
 * @brief		Creates a new unlinked AST node with a given list of tokens.
 *
 * @return		Pointer to the mallocated t_ast_node.
 */
t_ast_node	*create_ast_node(void)
{
	t_ast_node	*node;

	node = malloc(sizeof(t_ast_node));
	if (!node)
		return (ms_ptr_error(NULL, NULL, NULL, NULL));
	node->parent = NULL;
	node->left = NULL;
	node->right = NULL;
	node->tk_list = NULL;
	node->in_fd = STDIN_FILENO;
	node->out_fd = STDOUT_FILENO;
	node->pid = -1;
	return (node);
}

/**
 * @brief		Unlinks an AST node from its parent (if it exists), frees
 * 				all its memory and closes its file descriptors
 * 				(also if they exists).
 *
 * @param[in]	node	The mallocated t_ast_node pointer to free.
 * @return		NULL.
 */
t_ast_node	*free_ast_node(t_ast_node *node)
{
	if (node->parent)
	{
		if (node->parent->left == node)
			node->parent->left = NULL;
		if (node->parent->right == node)
			node->parent->right = NULL;
	}
	if (node->in_fd > 1 && close(node->in_fd))
		ms_put_error(NULL, NULL, NULL, 0);
	if (node->out_fd > 1 && close(node->out_fd))
		ms_put_error(NULL, NULL, NULL, 0);
	free_token_list(node->tk_list);
	free(node);
	return (NULL);
}

/**
 * @brief		Frees the whole AST from any given node in the tree.
 *
 * @param[in]	node	pointer to any node belonging to the ast.
 * @return		NULL
 */
t_ast_node	*free_whole_ast(t_ast_node *node)
{
	t_ast_node	*node_to_free;

	while (node)
	{
		if (node->left)
		{
			node = node->left;
			continue ;
		}
		if (node->right)
		{
			node = node->right;
			continue ;
		}
		node_to_free = node;
		node = node->parent;
		free_ast_node(node_to_free);
	}
	return (NULL);
}

/**
 * @brief		Creates a new linked pair of AST nodes with a parent and
 * 				a right leaf node, and inserts them as a node parent.
 *
 * @param[in]	node	The node above which to insert the new pair.
 *
 * @return		0 on success, 1 on error.
 */
int	insert_new_node_pair_upright(t_ast_node *node)
{
	t_ast_node	*pair_parent;

	pair_parent = create_ast_node();
	if (!pair_parent)
		return (1);
	pair_parent->right = create_ast_node();
	if (!pair_parent->right)
	{
		free_ast_node(pair_parent);
		return (1);
	}
	pair_parent->right->parent = pair_parent;
	pair_parent->left = node;
	if (node->parent)
	{
		pair_parent->parent = node->parent;
		if (node == node->parent->left)
			node->parent->left = pair_parent;
		if (node == node->parent->right)
			node->parent->right = pair_parent;
	}
	node->parent = pair_parent;
	return (0);
}

/**
 * @brief		Creates a new linked pair of AST nodes with a parent and
 * 				a right leaf node, and inserts them as the node left child.
 *
 * @param[in]	node	The node under which to insert the new pair.
 *
 * @return		0 on success, 1 on error.
 */
int	insert_new_node_pair_downleft(t_ast_node *node)
{
	t_ast_node	*pair_parent;

	pair_parent = create_ast_node();
	if (!pair_parent)
		return (1);
	pair_parent->right = create_ast_node();
	if (!pair_parent->right)
	{
		free_ast_node(pair_parent);
		return (1);
	}
	pair_parent->right->parent = pair_parent;
	pair_parent->parent = node;
	pair_parent->left = node->left;
	if (node->left)
		node->left->parent = pair_parent;
	node->left = pair_parent;
	return (0);
}
