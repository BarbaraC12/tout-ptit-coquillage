/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/18 16:37:01 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 09:27:46 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"
#include "minishell.h"

/**
 * @brief	Generates an error message for the unexpected next token
 *			in the list.
 *
 * @param	tk_list	Token list
 * @return	1
 */
int	unexpected_token(t_tk_list *tk_list)
{
	char	msg[50];
	int		len;

	if (!tk_list)
		ft_strcpy(msg, "syntax error near unexpected token `newline'");
	else if (tk_list->type == LX_TK_LITTERAL)
		ft_strcpy(msg, "syntax error near unexpected litteral token.");
	else
	{
		len = ft_strlcpy(msg, "syntax error near unexpected token `", 50);
		len += ft_strlcpy(msg + len, get_tk_prefix(tk_list->type), 50 - len);
		len += ft_strlcpy(msg + len, "'", 50 - len);
	}
	g_status = 2;
	return (ms_put_error(NULL, NULL, msg, 1));
}

/**
 * @brief			Tries to parse a command from a token list, given a valid
 * 					command node inside an AST, including parenthesis, arguments
 * 					and redirections.
 *
 * @param[in,out]	cmd_node	The actual AST command node to work on.
 * @param[in,out]	tk_input	The list of tokens to parse.
 * @return			0 on success, 1 on error.
 */
static int	parse_command(t_ast_node **cmd_node, t_tk_list **tk_input)
{
	int	err;

	if (parse_all_open_parenthesis(cmd_node, tk_input))
		return (1);
	if (!*tk_input || (*tk_input)->type > LX_TK_OUTPUT)
		return (unexpected_token(*tk_input));
	err = 0;
	while (!err && *tk_input && (*tk_input)->type <= LX_TK_OUTPUT)
	{
		if ((*tk_input)->type <= LX_TK_SPACE)
			err = append_tk_list(&(*cmd_node)->tk_list,
					detach_tk_group(tk_input));
		else
			err = parse_redirection(cmd_node, tk_input);
	}
	while (!err && *tk_input && (*tk_input)->type >= LX_TK_HEREDOC
		&& (*tk_input)->type <= LX_TK_PAR_CLOSE)
	{
		if ((*tk_input)->type == LX_TK_PAR_CLOSE)
			err = parse_closed_parenthesis(cmd_node, tk_input);
		else
			err = parse_redirection(cmd_node, tk_input);
	}
	return (err);
}

/**
 * @brief			Tries to parse a separator from a token list, modifies
 * 					a given AST accordingly while making a new command node.
 *
 * @param[in,out]	cmd_node	The actual AST command node to work on.
 * @param[in,out]	tk_input	The list of tokens to extract a separator
 * 								token from (with type >= LX_TK_PIPE).
 * @return			0 on success, 1 on error.
 */
static int	parse_separator(t_ast_node **cmd_node, t_tk_list **tk_input)
{
	t_ast_node	*top_node;

	remove_leading_space_tokens(tk_input);
	if (!*tk_input || (*tk_input)->type < LX_TK_PIPE)
		return (unexpected_token(*tk_input));
	if ((*cmd_node)->parent
		&& (*cmd_node)->parent->tk_list->type != LX_TK_PAR_OPEN)
		top_node = (*cmd_node)->parent;
	else
		top_node = *cmd_node;
	if (insert_new_node_pair_upright(top_node))
		return (1);
	if ((*tk_input)->type == LX_TK_PIPE)
		set_pipe_between(*cmd_node, top_node->parent->right);
	top_node->parent->tk_list = detach_first_token(tk_input);
	*cmd_node = top_node->parent->right;
	return (0);
}

/**
 * @brief			Builds an Abstract Syntax Tree from a given list of tokens.
 *
 * @param[in,out]	token_input		The list of tokens to buils the AST from.
 * 									This list is consumed by the AST creation.
 * @return			the root node of the AST, or NULL if any error occurs.
 */
t_ast_node	*build_ast(t_tk_list **token_input)
{
	t_ast_node		*cmd_node;

	if (!*token_input)
		return (NULL);
	cmd_node = create_ast_node();
	if (!cmd_node)
	{
		*token_input = free_token_list(*token_input);
		return (NULL);
	}
	if (parse_command(&cmd_node, token_input))
		return (free_whole_ast(cmd_node));
	while (*token_input)
	{
		if (parse_separator(&cmd_node, token_input)
			|| parse_command(&cmd_node, token_input))
			return (free_whole_ast(cmd_node));
	}
	while (cmd_node->parent)
		cmd_node = cmd_node->parent;
	return (cmd_node);
}
