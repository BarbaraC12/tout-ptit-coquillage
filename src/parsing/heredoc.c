/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heredoc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcano <bcano@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/26 06:03:50 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/28 20:04:36 by bcano            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <signal.h>
#include <readline/readline.h>
#include "minishell.h"
#include "libft.h"
#include "parsing.h"

#define HEREDOC_EOF_ERROR "here-document delimited by end-of-file."

static void	read_loop(char *stop, int write_fd)
{
	char	*line;

	write(1, stop, ft_strlen(stop));
	line = readline("| > ");
	while (g_status != 128 && line && ft_strcmp(line, stop))
	{
		if (write(write_fd, line, ft_strlen(line)) == -1
			|| write(write_fd, "\n", 1) == -1)
		{
			ms_put_error(NULL, NULL, NULL, -1);
			break ;
		}
		free(line);
		write(1, stop, ft_strlen(stop));
		line = readline("| > ");
	}
	if (g_status != 128 && !line)
		ms_put_error(NULL, "warning :"HEREDOC_EOF_ERROR" wanted ", stop, 0);
}

/**
 * @brief		Creates a piped heredoc which reads from stdin until the stop
 * 				value is meet.
 *
 * @param[in]	tk_group	Token group to parse the heredoc stop value from.
 * @return		valid file descriptor on success, -1 on error.
 */
int	create_heredoc(t_tk_list *tk_group)
{
	int		stdin_save;
	char	*stop;
	int		pipefd[2];

	stdin_save = dup(STDIN_FILENO);
	stop = unexpand_tk_list(tk_group);
	if (pipe(pipefd))
		return (ms_put_error(NULL, NULL, NULL, -1));
	signal(SIGINT, &heredoc_sighandler);
	read_loop(stop, pipefd[1]);
	if (close(pipefd[1]))
		ms_put_error(NULL, NULL, NULL, 0);
	if (g_status == 128)
		dup2(stdin_save, STDIN_FILENO);
	else
		close(stdin_save);
	return (free(stop), pipefd[0]);
}
