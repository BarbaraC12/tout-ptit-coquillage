/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jraffin <jraffin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/03 00:59:09 by jraffin           #+#    #+#             */
/*   Updated: 2022/01/26 07:49:29 by jraffin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"
#include "parsing.h"
#include "minishell.h"

void	set_pipe_between(t_ast_node *write_node, t_ast_node *read_node)
{
	int	pipe_fds[2];

	if (pipe(pipe_fds))
	{
		write_node->out_fd = -1;
		read_node->in_fd = -1;
		ms_put_error(NULL, NULL, NULL, 0);
	}
	else
	{
		write_node->out_fd = pipe_fds[1];
		read_node->in_fd = pipe_fds[0];
	}
}

/**
 * @brief			Tries to parse an open parenthesis from a token list, and
 * 					pushes it as a node above the given command node as its
 * 					parent.
 *
 * @param[in,out]	cmd_node	The actual AST command node to work on.
 * @param[in,out]	tk_input	The list of tokens to parse.
 * @return			0 on success, 1 on error.
 */
int	parse_all_open_parenthesis(t_ast_node **cmd_node, t_tk_list **tk_input)
{
	remove_leading_space_tokens(tk_input);
	while (*tk_input && (*tk_input)->type == LX_TK_PAR_OPEN)
	{
		(*cmd_node)->tk_list = detach_first_token(tk_input);
		(*cmd_node)->right = create_ast_node();
		if (!(*cmd_node)->right)
			return (1);
		(*cmd_node)->right->parent = *cmd_node;
		*cmd_node = (*cmd_node)->right;
		remove_leading_space_tokens(tk_input);
	}
	return (0);
}

/**
 * @brief			Tries to parse a redirection token from a token list, and
 * 					inserts it as a node pair under the given command node.
 * 					Also, if the redirection is a LX_TK_HEREDOC type, it creates
 * 					immediately the piped heredoc, reads its input, and stores
 * 					its file descriptor	in the redirection node.
 *
 * @param[in,out]	cmd_node	The actual AST command node to work on.
 * @param[in,out]	tk_input	The list of tokens to parse.
 * @return			0 on success, 1 on error.
 */
int	parse_redirection(t_ast_node **cmd_node, t_tk_list **tk_input)
{
	t_ast_node	*redirection_pair_root;

	remove_leading_space_tokens(tk_input);
	if (!*tk_input || (*tk_input)->type < LX_TK_HEREDOC
		|| (*tk_input)->type > LX_TK_OUTPUT)
		return (unexpected_token(*tk_input));
	if (insert_new_node_pair_downleft(*cmd_node))
		return (1);
	redirection_pair_root = (*cmd_node)->left;
	redirection_pair_root->tk_list = detach_first_token(tk_input);
	redirection_pair_root->right->tk_list = detach_tk_group(tk_input);
	if (!redirection_pair_root->right->tk_list)
		return (unexpected_token(*tk_input));
	if (redirection_pair_root->tk_list->type == LX_TK_HEREDOC)
	{
		redirection_pair_root->in_fd = create_heredoc(
				redirection_pair_root->right->tk_list);
		if (redirection_pair_root->in_fd == -1 || g_status == 128)
			return (1);
	}
	return (0);
}

/**
 * @brief			Tries to parse a closed parenthesis from a token list, and
 * 					if one is found, relocates the given command node to
 * 					the next parent LX_TK_PAR_OPEN node.
 *
 * @param[in,out]	cmd_node	The actual AST command node to work on.
 * @param[in,out]	tk_input	The list of tokens to parse.
 * @return			0 on success, 1 on error.
 */
int	parse_closed_parenthesis(t_ast_node **cmd_node, t_tk_list **tk_input)
{
	t_ast_node	*new_cmd_node;

	if (!*tk_input || (*tk_input)->type != LX_TK_PAR_CLOSE)
		return (unexpected_token(*tk_input));
	if (!(*cmd_node)->tk_list && !(*cmd_node)->left)
		return (unexpected_token(*tk_input));
	new_cmd_node = (*cmd_node)->parent;
	while (new_cmd_node && new_cmd_node->tk_list->type != LX_TK_PAR_OPEN)
		new_cmd_node = new_cmd_node->parent;
	if (!new_cmd_node)
		return (unexpected_token(*tk_input));
	free_token_list(detach_first_token(tk_input));
	remove_leading_space_tokens(tk_input);
	*cmd_node = new_cmd_node;
	return (0);
}
